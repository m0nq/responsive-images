(() => {
  const images = document.querySelectorAll('img');
  const sizes = {
    showcase: '110vw',
    reason: '(max-width: 799px) 100vw, 372px',
    feature: '(max-width: 799px) 100vw, 558px',
    story: '(max-width: 799px) 100vw, 670px'
  };

  const makeSrcset = imgSrc => {
    let markup = [];
    let width = 400;
    for (let i = 0; i < 5; i++) {
      markup[i] = `${imgSrc}-${width}.jpg ${width}w`;
      width += 400;
    }
    return markup.join();
  };

  images.forEach(image => {
    let imgSrc = image.getAttribute('src');
    imgSrc = imgSrc.slice(0, -8);
    let srcset = makeSrcset(imgSrc);
    image.setAttribute('srcset', srcset);
    let type = image.getAttribute('data-type');
    let sizeType = sizes[type];
    image.setAttribute('sizes', sizeType);
  });
})();
